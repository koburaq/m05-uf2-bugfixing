package com.company;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        int userAge, feedback;
        String userInput;

        System.out.println("I am ready to play!");

        System.out.println("What's your age?");
        userAge = input.nextInt();

        if (userAge < 13) {
            System.out.println("You are allowed to play at your own risk");

        } else {
            System.out.println("Play On!");
        }

        System.out.println("You are at an Iggy concert, and you hear this lyric 'Are you ready?, start running.'");
        System.out.println("Suddenly, Iggy stops and says, 'Who wants to race me at running?'\n");

        System.out.println("Do you want to race Iggy on stage?");
        userInput = input.next(); // ------????

        if(userInput.equals("yes")){
            System.out.println("You and Iggy start racing. It's neck and neck! You win by a shoelace!");

        } else {
            System.out.println("Oh no! Iggy shakes his head and sings 'I set a pace, so I can race without pacing.'");

        }

        System.out.println("Rate your game out of 10:");
        feedback = input.nextInt();

        if(feedback > 8){
            System.out.println("Thank you! You should race again at the next concert!");

        }else{
            System.out.println("I'll keep practicing coding and racing.");

        }


    }
}